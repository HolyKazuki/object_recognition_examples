#! /usr/bin/python

import sys
import numpy as np
import cv2 as cv

img = cv.imread(sys.argv[1])

saliency = cv.saliency.ObjectnessBING_create()
saliency.setTrainingPath("/usr/local/share/OpenCV/samples/saliency/ObjectnessTrainedModel")

(success, bboxes) = saliency.computeSaliency(img)

bboxes = bboxes.reshape((-1,4))

for i in xrange(bboxes.shape[0]):
    print "BBox:", i
    minx, miny, maxx, maxy = bboxes[i]
    disp_img = img.copy()
    cv.rectangle(disp_img, (minx, miny), (maxx, maxy), (0,0,255), 2)

    cv.imshow("display", disp_img)
    cv.waitKey(0)