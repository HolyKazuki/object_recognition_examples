#! /usr/bin/python

import sys
import numpy as np
import cv2 as cv

img = cv.imread(sys.argv[1])

saliency = cv.saliency.StaticSaliencySpectralResidual_create()

(success, saliencyMap) = saliency.computeSaliency(img)

saliencyMap = (saliencyMap * 255).astype("uint8")
saliencyMap = cv.cvtColor(saliencyMap,cv.COLOR_GRAY2RGB)
display = np.concatenate((img, saliencyMap), axis=1)
cv.imshow("display", display)
cv.waitKey(0)