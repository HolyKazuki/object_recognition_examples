#! /usr/bin/python

import sys
import cv2 as cv
import numpy as np
import random
from matplotlib import pyplot as plt

def findAffineTransformRANSAC(
    points1,
    points2,
    inlier_dist_thresh = 2.0,
    min_num_inliers = 20,
    num_iters = 100
):
    points1 = np.array(points1)
    points2 = np.array(points2)

    if points1.shape[0] < 3 or points2.shape[0] < 3 or points1.shape[0] != points2.shape[0]:
        raise RuntimeError("Too few points or mismatched point lists sent to findAffineTransformRANSAC")

    inner_points1 = np.zeros((3,2), dtype=np.float32)
    inner_points2 = np.zeros((3,2), dtype=np.float32)
    inlier_count = 0

    points2_from_points1 = None

    for i in xrange(num_iters):
        #Determine an affine transform using 3 random points
        for inner_pt_index in xrange(3):
            pt_index = random.randint(0,points1.shape[0]-1)

            inner_points1[inner_pt_index] = points1[pt_index]
            inner_points2[inner_pt_index] = points2[pt_index]
        
        inner_points2_from_points1 = cv.getAffineTransform(inner_points1, inner_points2)

        #Check for inliers using this transform
        trans_points1 = points1.reshape((1,-1,2))
        est_points2 = cv.transform(trans_points1,inner_points2_from_points1)
        est_points2 = est_points2.reshape((-1,2))
        
        inner_inlier_count = 0
        for pt_index in xrange(points2.shape[0]):
            dist = cv.norm(est_points2[pt_index]-points2[pt_index])
            if dist < inlier_dist_thresh:
                inner_inlier_count += 1
        
        if inner_inlier_count >= min_num_inliers and inner_inlier_count > inlier_count:
            inlier_count = inner_inlier_count
            points2_from_points1 = inner_points2_from_points1

    return inlier_count, points2_from_points1

target_img = cv.imread(sys.argv[1])
search_img = cv.imread(sys.argv[2])

#extractor = cv.ORB_create()
#extractor = cv.BRISK_create()
#extractor = cv.AKAZE_create()
extractor = cv.xfeatures2d.SIFT_create()

target_kpts, target_descs = extractor.detectAndCompute(target_img, None)
search_kpts, search_descs = extractor.detectAndCompute(search_img, None)

matcher = cv.BFMatcher(cv.NORM_L2, crossCheck=False)

matches = matcher.match(target_descs, search_descs)

target_pts = [target_kpts[match.queryIdx].pt for match in matches]
search_pts = [search_kpts[match.trainIdx].pt for match in matches]
inlier_count, points2_from_points1 = findAffineTransformRANSAC(target_pts, search_pts)

print "Inlier count:", inlier_count
print "Transform:"
print points2_from_points1

if points2_from_points1 is not None:
    target_border_pts = [
        [0,0],
        [0,target_img.shape[0]],
        [target_img.shape[1],target_img.shape[0]],
        [target_img.shape[1],0],
    ]

    target_border_pts = np.array(target_border_pts).reshape((1,-1,2))
    search_border_pts = cv.transform(target_border_pts, points2_from_points1)
    search_border_pts = search_border_pts.reshape((-1,2))

    for i in xrange(search_border_pts.shape[0]):
        pt1 = search_border_pts[i]
        pt2 = search_border_pts[(i+1)%search_border_pts.shape[0]]
        pt1 = tuple(pt1.tolist())
        pt2 = tuple(pt2.tolist())
        cv.line(search_img, pt1, pt2, [0,0,255], 5)

    target_pts = np.array(target_pts).reshape((1,-1,2))
    test_search_pts = cv.transform(target_pts, points2_from_points1)
    test_search_pts = test_search_pts.reshape((-1,2))

    inlier_matches = []
    for i in xrange(len(matches)):
        dist = cv.norm(test_search_pts[i]-search_pts[i])
        if dist < 2.0:
            inlier_matches.append(matches[i])
    matches = inlier_matches

display_img = cv.drawMatches(target_img, target_kpts, search_img, search_kpts, matches, None, flags=2)

cv.namedWindow("display", cv.WINDOW_NORMAL)
cv.imshow("display", display_img)
cv.waitKey(0)