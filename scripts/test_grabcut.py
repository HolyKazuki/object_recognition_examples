#! /usr/bin/python

import sys
import numpy as np
import cv2 as cv

img = cv.imread(sys.argv[1])

rect = cv.selectROI(img)

mask = np.zeros(img.shape[:2],np.uint8)
bgdModel = np.zeros((1,65),np.float64)
fgdModel = np.zeros((1,65),np.float64)

cv.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv.GC_INIT_WITH_RECT)
mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
img = img*mask2[:,:,np.newaxis]

cv.imshow("img",img)
cv.waitKey(0)

